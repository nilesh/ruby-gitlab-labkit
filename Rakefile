# frozen_string_literal: true

require "bundler/gem_tasks"
require "rufo"

begin
  require "rspec/core/rake_task"
  RSpec::Core::RakeTask.new(:spec)
end

require "rubocop/rake_task"
RuboCop::RakeTask.new

desc "Alias for `rake rufo:run`"
task :format => ["rufo:run"]

namespace :rufo do
  require "rufo"

  def rufo_command(*switches, rake_args)
    files_or_dirs = rake_args[:files_or_dirs] || "."
    args = switches + files_or_dirs.split(" ")
    Rufo::Command.run(args)
  end

  desc "Format Ruby code in current directory"
  task :run, [:files_or_dirs] do |_task, rake_args|
    rufo_command(rake_args)
  end

  desc "Check that no formatting changes are produced"
  task :check, [:files_or_dirs] do |_task, rake_args|
    rufo_command("--check", rake_args)
  end
end

desc "Generate test protobuf stubs"
task :gen_test_proto do
  system "grpc_tools_ruby_protoc --ruby_out=. --grpc_out=. spec/support/grpc_service/test.proto"
  Rufo::Command.run(["spec/support/grpc_service/test_pb.rb", "spec/support/grpc_service/test_services_pb.rb"])
end

task :fix => %w[rufo:run rubocop:auto_correct]

task :verify => %w[spec rufo:check rubocop]

task :default => %w[verify build]
