﻿# LabKit-Ruby 🔬🔬🔬🔬🔬

LabKit-Ruby is minimalist library to provide functionality for Ruby services at GitLab.

LabKit-Ruby is the Ruby companion for [LabKit](https://gitlab.com/gitlab-org/labkit), a minimalist library to provide functionality for Go services at GitLab.

LabKit-Ruby and LabKit are intended to provide similar functionality, but use the semantics of their respective languages, so are not intended to provide identical APIS.

## Documentation

API Documentation is available at [the Rubydoc site](https://www.rubydoc.info/gems/gitlab-labkit/).

## Changelog

The changelog is available via [**tagged release notes**](https://gitlab.com/gitlab-org/labkit-ruby/tags)

## Functionality

LabKit-Ruby provides functionality in three areas:

1. `Labkit::Correlation` for handling and propagating Correlation-IDs.
1. `Labkit::Logging` for sanitizing log messages.
1. `Labkit::Tracing` for handling and propagating distributed traces.

## Developing

Anyone can contribute!

```console
$ git clone git@gitlab.com:gitlab-org/labkit-ruby.git
$ cd labkit-ruby
$ bundle install

$ # Autoformat code and auto-correct linters
$ bundle exec rake fix

$ # Run tests, linters
$ bundle exec rake verify
```

Note that LabKit-Ruby uses the [`rufo`](https://github.com/ruby-formatter/rufo) for auto-formatting. Please run `bundle exec rake fix` to auto-format your code before pushing.

Please also review the [development section of the LabKit (go) README](https://gitlab.com/gitlab-org/labkit#developing-labkit) for details of the LabKit architectural philosophy.
